import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.util.List;
import java.util.Random;

public class FromTest extends BaseTest {
    Random rd = new Random();

    public  int randomize(int i){
        int rndNumber = rd.nextInt(i);
        System.out.println("Randomed number: " + rndNumber);
        return rndNumber;
    }

    @Test
    public void firstTest() throws InterruptedException {
        driver.get("http://toolsqa.com/automation-practice-form/");
        WebElement firstName = driver.findElement(By.name("firstname"));
        firstName.sendKeys("Jan");
        WebElement secondName = driver.findElement(By.name("lastname"));
        secondName.sendKeys("Kwiatek");
      //  List<WebElement> sex = driver.findElements(By.name("sex"));
        List<WebElement> sex = driver.findElements(By.cssSelector("input[id*='sex']"));
        sex.get(0).click();

        Assert.assertTrue(sex.get(0).isSelected());

        int i = rd.nextInt(6);
        List<WebElement> yersOfExpirience = driver.findElements(By.cssSelector("input[id*=exp"));
        yersOfExpirience.get(i).click();
        Assert.assertTrue(yersOfExpirience.get(i).isSelected());

        WebElement date = driver.findElement(By.id("datepicker"));
        date.sendKeys(":2010-05-10");

        List<WebElement> profesional = driver.findElements(By.cssSelector("input[id*='profession']"));
        profesional.get(0).click(); // clik odpowiednio klika argument

        WebElement uploadPhoto = driver.findElement(By.id("photo")); // pobieramy element
       // uploadPhoto.sendKeys("C:\\drivers\\face.png"); <-- sciezka do dysku
        File photo = new File("src\\files\\face.png");
        String absolutePathToPhoto = photo.getAbsolutePath();
        System.out.println("absolute path: " + absolutePathToPhoto);
        uploadPhoto.sendKeys(absolutePathToPhoto);


      WebElement downloadFile = driver.findElement(By.linkText("Selenium Automation Hybrid Framework"));
        downloadFile.click();

        WebElement downloadFile2 = driver.findElement(By.linkText("Test File to Download"));
        downloadFile2.click();


        List<WebElement>  automaticTool = driver.findElements(By.cssSelector("input[id*=tool"));
        automaticTool.get(0).click();
        Assert.assertTrue(yersOfExpirience.get(i).isSelected());


        WebElement countryList = driver.findElement(By.id("continents"));
        Select countrySelect = new Select(countryList);

        //pobieranie ilości dostępnych opcji
        int numberOfOption = countrySelect.getOptions().size();
        //wylosyanie liczby z zakresu 0 do liczbyOpcji-1
        //   i= rd.nextInt(numberOfOption-1);
        i= randomize(numberOfOption-1);
           //klikniecie losowej opcji
        countrySelect.selectByIndex(i);

       // countrySelect.selectByVisibleText("Europe");


     //   WebElement commandsList = driver.findElement(By.id(""));


        Thread.sleep(3000); // czeka 3 sekundy
    }

}

//https://www.kainos.pl/blog/zaawansowane-interakcje-w-selenium-webdriver/